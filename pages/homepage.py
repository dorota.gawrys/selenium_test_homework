from selenium.webdriver.common.by import By



class HomePage:

    def __init__(self, browser):
        self.browser = browser

    def click_logout(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()

    def click_on_administration_link(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin a').click()

    def click_tools_icon(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

