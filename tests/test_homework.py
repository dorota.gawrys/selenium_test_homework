import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.add_project_page import AddProjectPage
from pages.homepage import HomePage
from pages.login_page import LoginPage

administrator_email = 'administrator@testarena.pl'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    # coś do przekazujemy do testu
    yield browser
    # część która wykona się po każdym teście
    browser.quit()

@pytest.fixture()
def the_project_website(browser):
    the_project_website = AddProjectPage(browser)
    the_project_website.click_add_project()
    yield browser
    browser.quit()

def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email


def test_testarena_logout(browser):
    home_page = HomePage(browser)
    home_page.click_logout()

    assert '/zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, '#password').is_displayed()
def test_new_project(the_project_website):
    element = the_project_website.find_element(By.CSS_SELECTOR, 'td:nth-child(1)')
    assert element.is_displayed()
