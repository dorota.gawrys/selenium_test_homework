import pytest

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.add_project_page import AddProjectPage
from pages.homepage import HomePage
from pages.login_page import LoginPage
from pages.random_string import get_random_string

administrator_email = 'administrator@testarena.pl'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    # coś do przekazujemy do testu
    yield browser
    # część która wykona się po każdym teście
    browser.quit()


def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email


def test_testarena_logout(browser):
    home_page = HomePage(browser)
    home_page.click_logout()

    assert '/zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, '#password').is_displayed()


def test_new_project(browser):

    arena_home_page = HomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = AddProjectPage(browser)
    arena_project_page.verify_title('Projekty')

    arena_home_page = AddProjectPage(browser)
    arena_home_page.click_add_project()
    assert browser.current_url == 'http://demo.testarena.pl/administration/add_project'

    arena_home_page = AddProjectPage(browser)
    random_text = get_random_string()
    arena_home_page.insert_random_name(random_text)
    arena_home_page.click_save_project()







